angular.module('wallApp', ['ui.bootstrap', 'ui.router', 'ngRoute', 'ngResource', 'ngCookies']).constant(
  'backendHost', 'http://localhost:5000/'
).config(
  function($stateProvider, $routeProvider, $httpProvider) {
    $httpProvider.interceptors.push('urlRewrite');
    $httpProvider.interceptors.push('unAuth');
    $routeProvider.otherwise('/');
    $stateProvider.state(
      'deletePost', {
        templateUrl: 'views/deletePost.html',
        controller: 'deletePostCtrl',
        url: '/posts/delete/:id'
      }
    );
    $stateProvider.state(
      'editPost', {
        templateUrl: 'views/editPost.html',
        controller: 'editPostCtrl',
        url: '/posts/edit/:id'
      }
    );
    $stateProvider.state(
      'home', {
        templateUrl: 'views/home/home.html',
        controller: 'homeCtrl',
        url: '/'
      }
    );
    $stateProvider.state(
      'login', {
        templateUrl: 'views/login.html',
        controller: 'loginCtrl',
        url: '/login'
      }
    );
    $stateProvider.state(
      'logout', {
        templateUrl: 'views/logout.html',
        controller: 'logoutCtrl',
        url: '/logout'
      }
    );
    $stateProvider.state(
      'register', {
        templateUrl: 'views/register.html',
        controller: 'registerCtrl',
        url: '/register'
      }
    );
  }
).run(
  function($rootScope, $state, $stateParams, $cookies, $http) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $rootScope.isLoggedIn = function() {
      return !!$cookies.get('token');
    };
    $rootScope.logout = function() {
      $rootScope.user = null;
      $cookies.remove('token');
      $state.go('login');
    };
    if($rootScope.isLoggedIn()) {
      $http.post('api/users/current').then(
        function(response) {
          $rootScope.user = response.data;
        }
      );
    }
    $state.go('home');
  }
);
