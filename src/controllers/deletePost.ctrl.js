angular.module('wallApp').controller(
  'deletePostCtrl',
  function(notify, $scope, $state, postFactory, $stateParams) {
    var vm = this;
    
    $scope.vm = vm;

    vm.post = postFactory.get({id: $stateParams['id']});

    $scope.deletePost = function() {
      postFactory.remove({id: vm.post.id}).$promise.then(
        function(response) {
          $state.go('home');
        },
        function(response) {
          if(response.status == 401) {
            notify.error('Kindly login first.');
            return;
          }
          notify.error('Error occurred while deleting post.');
        }
      );
    };
  }
);
