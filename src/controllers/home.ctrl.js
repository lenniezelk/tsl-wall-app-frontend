angular.module('wallApp').controller(
  'homeCtrl',
  function(notify, $scope, $state, postFactory) {
    var vm = this;
    
    vm.posts = postFactory.query();

    $scope.vm = vm;

    $scope.sendPost = function() {
      var postData = {body: vm.postContent};
      postFactory.save(postData,
        function(response) {
          window.location = '/';
        },
        function(response) {
          if(response.status == 401) {
            notify.error('Kindly login first.');
            return;
          }
          notify.error('Error occurred while adding post.');
        }
      );
    }
  }
);
