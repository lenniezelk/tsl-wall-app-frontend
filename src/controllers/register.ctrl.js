angular.module('wallApp').controller(
  'registerCtrl',
  function($scope, $http, $rootScope, $cookies) {
    var vm = this;

    if($scope.user) {
      $scope.$state.go('home');
    }

    vm.credentials = {};

    $scope.registerUser = function() {
      $http.post(
        'api/users/create',
        vm.credentials
      ).then(
        function(response) {
          $rootScope.user = response.data.user;
          $cookies.put('token', response.data.token);
          vm.messages = null;
          $scope.$state.go('home');
        },
        function(response) {
          $rootScope.user = null;

          $cookies.remove('token');

          vm.messages = response.data;
        }
      );
    }
  
  $scope.vm = vm;
  }
);
