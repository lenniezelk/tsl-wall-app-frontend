angular.module('wallApp').controller(
  'editPostCtrl',
  function(notify, $scope, $state, postFactory, $stateParams) {
    var vm = this;
    
    $scope.vm = vm;

    vm.post = postFactory.get({id: $stateParams['id']});

    $scope.editPost = function() {
      postFactory.update(vm.post).$promise.then(
        function(response) {
          $state.go('home');
        },
        function(response) {
          if(response.status == 401) {
            notify.error('Kindly login first.');
            return;
          }
          notify.error('Error occurred while editing post.');
        }
      );
    };
  }
);
