angular.module('wallApp').controller(
  'loginCtrl',
  function($scope, $http, $rootScope, $cookies) {
    var vm = this;

    if($scope.user) {
      $scope.$state.go('home');
    }

    vm.credentials = {};

    $scope.loginUser = function() {
      $http.post(
        'api/auth/login',
        vm.credentials
      ).then(
        function(response) {
          $rootScope.user = response.data.user;
          $cookies.put('token', response.data.token);
          vm.messages = null;
          $scope.$state.go('home');
        },
        function(response) {
          $rootScope.user = null;

          if(response.status == 401) {
            vm.messages = {
              'email and/or password': 'invalid'
            };

            $cookies.remove('token');

            return;
          }

          vm.messages = response.data;
        }
      );
    }
  
  $scope.vm = vm;
  }
);
