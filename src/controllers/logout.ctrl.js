angular.module('wallApp').controller(
  'logoutCtrl',
  function($rootScope, $scope, $state, $http, $cookies) {
    $http.post(
      'api/auth/logout'
    ).then(
      function() {
        $rootScope.user = null;
        $cookies.remove('token');
        $state.go('home');
      }
    );
  }
);
