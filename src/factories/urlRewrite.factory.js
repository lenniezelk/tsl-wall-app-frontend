angular.module('wallApp').factory(
  'urlRewrite',
  function(backendHost, $cookies) {
    return {
      request: function(config) {
        if(config.url.startsWith('api/')) {
          config.url = backendHost + config.url;
        }

        if(config.method == 'DELETE' || config.method == 'POST' || config.method == 'PUT') {
          config.headers = angular.extend({}, config.headers, {
            Authorization: 'Token ' + $cookies.get('token')
          });
        }

        return config;
      }
    };
  }
);
