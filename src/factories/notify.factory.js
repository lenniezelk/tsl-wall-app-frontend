angular.module('wallApp').factory(
  'notify',
  function() {
    var notifyMsg = function(msg, type) {
      noty(
        {
          text: msg,
          type: type
        }
      );
    };
    return {
      error: function(message) {
        notifyMsg(message, 'error');
      },
      success: function(message) {
        notifyMsg(message, 'success');
      }
    };
  }
);
