angular.module('wallApp').factory(
  'unAuth',
  function($q, $rootScope) {
    return {
      responseError: function(response) {
        if(response.status == 401) {
          $rootScope.logout();
        }
        return $q.reject(response);
      }
    };
  }
);
