angular.module('wallApp').factory(
  'postFactory',
  function($resource) {
    return $resource(
      'api/posts/:id/ ',
      {'id': '@id'},
      {update: {method: 'PUT'}}
    );
  }
);
