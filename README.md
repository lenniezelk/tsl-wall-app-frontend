# Wall App frontend #

https://docs.google.com/document/d/1mCsQUHatIj7gEcGNq6Q3FurGotUHmu-HLtnc9Fe_vBc/edit#

### What is this repository for? ###

* Front end app for Wall App

### How do I get set up? ###

* Ensure `node` and `npm` are installed
* Install `gulp` and `bower` globally: `sudo npm install -g bower gulp`
* Clone this repository and `cd` into project directory
* Run `npm install` and `bower install`
* Run `gulp`
* Run `gulp serve`
* In your browser visit `http://localhost:9000`