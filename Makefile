env=development
process=all
group=all
tags=all


ANSIBLE = ansible $(group) -i devops/inventory/$(env)
ANSIBLE_PLAYBOOK = ansible-playbook -i devops/inventory/$(env)

# Match any playbook in the devops directory
% :: devops/%.yml 
	$(ANSIBLE_PLAYBOOK) $< -l $(group)
	
staging :
	$(eval env = staging )
	$(ANSIBLE_PLAYBOOK) devops/deploy.yml

bower:
	$(ANSIBLE) -m command -a "bower install path=/vagrant"

bower-module:
	$(ANSIBLE) -m command -a "bower install $(module) --config.cwd=/vagrant --save"

build-project:
	$(ANSIBLE) -m command -a "gulp --cwd /vagrant"

copy-views:
	$(ANSIBLE) -m command -a "gulp copy-views --cwd /vagrant"

npm-install:
	$(ANSIBLE) -m command -a "npm install --save-dev --prefix /vagrant $(module)"
