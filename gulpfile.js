// Include gulp
var gulp = require('gulp');
var wiredep = require('wiredep');

// Include plugins
var plugins = require('gulp-load-plugins')();

var jsSrc = 'src/**/*.js';

var cssSrc = 'src/assets/css/*.css';

var destJs = 'build/js';

var destCss = 'build/css';

gulp.task('copy-views', function(){
  return gulp.src(['src/views/**/*']).pipe(
    gulp.dest('build/views')
  );
});

gulp.task('scripts', function() {
    return gulp.src(jsSrc)
      .pipe(plugins.concat('main.js'))
        .pipe(plugins.rename({suffix: '.min'}))
        .pipe(plugins.ngAnnotate())
        .pipe(plugins.uglify())
        .pipe(gulp.dest(destJs));
});

gulp.task('css', function() {
    return gulp.src(cssSrc)
      .pipe(plugins.concat('main.css'))
        .pipe(plugins.rename({suffix: '.min'}))
        .pipe(plugins.uglifycss())
        .pipe(gulp.dest(destCss));
});

gulp.task('vendor-scripts', function() {
    
      return gulp.src(wiredep().js)
    
        .pipe(gulp.dest('build/vendor'));
    
    });
    
gulp.task('vendor-css', function() {
    
      return gulp.src(wiredep().css)
    
        .pipe(gulp.dest('build/vendor'));
    
    });

gulp.task('index', ['scripts', 'css', 'vendor-scripts', 'vendor-css'], function() {
    
      return gulp.src('src/index.html')
        .pipe(wiredep.stream({
          fileTypes: {
            html: {
              replace: {
                js: function(filePath) {
                  return '<script src="' + 'vendor/' + filePath.split('/').pop() + '"></script>';
                },
                css: function(filePath) {
                  return '<link rel="stylesheet" href="' + 'vendor/' + filePath.split('/').pop() + '"/>';
                }
              }
            }
          }
        }))
    
        .pipe(plugins.inject(
          gulp.src(['build/js/**/*.js'], { read: false }), {
            addRootSlash: false,
            transform: function(filePath, file, i, length) {
              return '<script src="' + filePath.replace('build/', '') + '"></script>';
            }
          }))
    
        .pipe(plugins.inject(
          gulp.src(['build/css/**/*.css'], { read: false }), {
            addRootSlash: false,
            transform: function(filePath, file, i, length) {
              return '<link rel="stylesheet" href="' + filePath.replace('build/', '') + '"/>';
            }
          }))
    
        .pipe(gulp.dest('build'));
    });

gulp.task('serve', plugins.serve({
  root: ['build'],
  port: 9000,
  hostname: '0.0.0.0'
}));

 // Default Task
gulp.task('default', ['css', 'scripts', 'vendor-scripts', 'vendor-css', 'copy-views', 'index']);
